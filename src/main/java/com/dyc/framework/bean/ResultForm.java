package com.dyc.framework.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class ResultForm<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean status;
	
	private T result;
	
	private String message;

	public ResultForm(Boolean status, T result, String message) {
		super();
		this.status = status;
		this.result = result;
		this.message = message;
	}
	
	public static <T> ResultForm<T> createSuccessResultForm(T result, String message) {
		return new ResultForm<T>(Boolean.TRUE, result, message);
	}
	
	public static <T> ResultForm<T> createErrorResultForm(T result, String message) {
		return new ResultForm<T>(Boolean.FALSE, result, message);
	}
	
}
