package com.dyc.course.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.dyc.framework.bean.ResultForm;

@RestController
@RequestMapping("/user")
public class UserController {

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResultForm<?> login(@RequestBody JSONObject jsonObject) {
		JSONObject result = new JSONObject();
		result.put("code", 20000);
		result.put("data", jsonObject.get("username"));
		return ResultForm.createSuccessResultForm(result, "success");
	}

	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public ResultForm<?> info(@RequestBody JSONObject jsonObject) {
		JSONObject info = new JSONObject();
		
		JSONObject result = new JSONObject();
		result.put("code", 20000);
		result.put("data", info);
		return ResultForm.createSuccessResultForm(null, "success");
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResultForm<?> logout() {
		JSONObject result = new JSONObject();
		result.put("code", 20000);
		result.put("data", "success");
		return ResultForm.createSuccessResultForm(null, "success");
	}
	
}
