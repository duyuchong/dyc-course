package com.dyc.course.common.exception;

/**
 * 错误代码定义
 *
 * @author qd-hz
 */
public interface RRExceptionCode {

    // 用户认证，所有需要重新登录的异常代码都必须在400开头
    /** 没有token */
    int NO_TOKEN = 400001;
    /** token失效 */
    int TOKEN_INVALID = 400002;

}
