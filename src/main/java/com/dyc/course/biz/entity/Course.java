package com.dyc.course.biz.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;

import com.dyc.course.entity.BaseEntity;
import com.dyc.course.sys.entity.SysDeptEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "course")
public class Course extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	private Long teacherId;
	
	private String description;

	/**
	 * 创建时间
	 */
	@CreatedDate
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

    @ManyToOne
    @JoinColumn(name="dept_id")
    private SysDeptEntity dept;

	/**
	 * 部门ID
	 */
	@Transient
	private Long deptId;

	@Transient
	private String deptName;

}
