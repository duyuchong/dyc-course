package com.dyc.course.biz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.biz.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {

	Page<Course> findByNameLike(String name, Pageable pageable);
	
}
