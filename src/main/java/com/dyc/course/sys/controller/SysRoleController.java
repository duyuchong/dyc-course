package com.dyc.course.sys.controller;

import com.dyc.course.common.annotation.SysLog;
import com.dyc.course.common.utils.R;
import com.dyc.course.sys.entity.SysRoleEntity;
import com.dyc.course.sys.mapper.SysRoleDeptMapper;
import com.dyc.course.sys.mapper.SysRoleMenuMapper;
import com.dyc.course.sys.repository.SysRoleRepository;
import com.dyc.course.sys.service.SysRoleService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

/**
 * 角色管理
 *
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
	
	@Autowired
	private SysRoleRepository repository;
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysRoleMenuMapper sysRoleMenuRepository;
	
	@Autowired
	private SysRoleDeptMapper sysRoleDeptRepository;
	
	/**
	 * 角色列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("sys:role:list")
	public R list(@RequestParam Map<String, Object> params) {
		String page = (String) params.get("page");
		String size = (String) params.getOrDefault("limit", "10");
		String sortBy = (String) params.getOrDefault("sortBy", "roleId");
		Sort sort = Sort.by(sortBy);
		PageRequest pageRequest = PageRequest.of(Integer.parseInt(page)-1, Integer.parseInt(size), sort);
		Page<SysRoleEntity> entities;
		if (params.containsKey("roleName") && params.get("roleName").toString().length() > 0) {
			entities = repository.findByRoleNameLike("%" + (String) params.get("roleName") + "%", pageRequest);
		} else {
			entities = repository.findAll(pageRequest);
		}
		return R.ok().put("page", entities);
	}
	
	/**
	 * 角色列表
	 */
	@RequestMapping("/select")
	@RequiresPermissions("sys:role:select")
	public R select() {
		List<SysRoleEntity> list = repository.findAll();
		
		return R.ok().put("list", list);
	}
	
	/**
	 * 角色信息
	 */
	@RequestMapping("/info/{roleId}")
	@RequiresPermissions("sys:role:info")
	public R info(@PathVariable("roleId") Long roleId) {
		Optional<SysRoleEntity> optional = repository.findById(roleId);
		SysRoleEntity role = null;
		if (optional.isPresent()) {
			role = optional.get();
		
			//查询角色对应的菜单
			List<Long> menuIdList = sysRoleMenuRepository.queryMenuIdList(roleId);
			role.setMenuIdList(menuIdList);
	
			//查询角色对应的部门
			List<Long> deptIdList = sysRoleDeptRepository.queryDeptIdList(new Long[]{roleId});
			role.setDeptIdList(deptIdList);
		}
		return R.ok().put("role", role);
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@RequestMapping("/save")
	@RequiresPermissions("sys:role:save")
	public R save(@RequestBody @Valid SysRoleEntity role){
		
		sysRoleService.saveRole(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@RequestMapping("/update")
	@RequiresPermissions("sys:role:update")
	public R update(@RequestBody @Valid SysRoleEntity role){
		
		sysRoleService.update(role);
		
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@RequestMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public R delete(@RequestBody Long[] roleIds) {
		sysRoleService.deleteBatch(roleIds);
		
		return R.ok();
	}
}
