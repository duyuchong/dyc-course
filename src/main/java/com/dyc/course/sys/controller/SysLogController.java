package com.dyc.course.sys.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dyc.course.common.utils.R;
import com.dyc.course.sys.entity.SysLogEntity;
import com.dyc.course.sys.repository.SysLogRepository;

/**
 * 系统日志
 *
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {
	
	@Autowired
	private SysLogRepository repository;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping("/list")
	@RequiresPermissions("sys:log:list")
	public R list(@RequestParam Map<String, Object> params) {
		String page = (String) params.get("page");
		String size = (String) params.getOrDefault("limit", "10");
		String sortBy = (String) params.getOrDefault("sortBy", "id");
		Sort sort = Sort.by(sortBy);
		PageRequest pageRequest = PageRequest.of(Integer.parseInt(page)-1, Integer.parseInt(size), sort);
		Page<SysLogEntity> entities;
		if (params.containsKey("username") && params.get("username").toString().length() > 0) {
			entities = repository.findByUsernameLike("%" + (String) params.get("username") + "%", pageRequest);
		} else {
			entities = repository.findAll(pageRequest);
		}
		return R.ok().put("page", entities);
	}
	
}
