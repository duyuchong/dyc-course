package com.dyc.course.sys.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface SysUserRoleMapper {

	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
	
}
