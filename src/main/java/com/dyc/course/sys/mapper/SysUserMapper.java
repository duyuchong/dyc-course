package com.dyc.course.sys.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface SysUserMapper {

	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

}
