package com.dyc.course.sys.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleMenuMapper {
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
	
}
