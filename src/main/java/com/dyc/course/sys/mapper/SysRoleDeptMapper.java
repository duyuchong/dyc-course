package com.dyc.course.sys.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleDeptMapper {

	/**
	 * 根据角色ID，获取部门ID列表
	 */
	List<Long> queryDeptIdList(Long[] roleIds);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
	
}
