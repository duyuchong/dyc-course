package com.dyc.course.sys.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysRoleEntity;

public interface SysRoleRepository extends JpaRepository<SysRoleEntity, Long> {

	Page<SysRoleEntity> findByRoleNameLike(String roleName, Pageable pageable);
	
}
