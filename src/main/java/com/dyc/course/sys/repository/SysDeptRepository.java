package com.dyc.course.sys.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysDeptEntity;

public interface SysDeptRepository extends JpaRepository<SysDeptEntity, Long> {

	List<SysDeptEntity> findByParentId(Long parentId);
	
}
