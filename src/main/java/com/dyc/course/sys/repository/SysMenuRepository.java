package com.dyc.course.sys.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dyc.course.sys.entity.SysMenuEntity;

@Repository
public interface SysMenuRepository extends JpaRepository<SysMenuEntity, Long> {

	List<SysMenuEntity> findByParentIdOrderByOrderNum(Long id);
	
	List<SysMenuEntity> findByTypeNot(Integer type);
	
}
