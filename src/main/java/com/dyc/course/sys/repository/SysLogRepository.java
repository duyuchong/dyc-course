package com.dyc.course.sys.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysLogEntity;

public interface SysLogRepository extends JpaRepository<SysLogEntity, Long> {

	Page<SysLogEntity> findByUsernameLike(String username, Pageable pageable);
	
}
