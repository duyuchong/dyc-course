package com.dyc.course.sys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.dyc.course.sys.entity.SysUserRoleEntity;

public interface SysUserRoleRepository extends JpaRepository<SysUserRoleEntity, Long> {

	@Transactional
	void deleteByUserId(Long userId);
	
}
