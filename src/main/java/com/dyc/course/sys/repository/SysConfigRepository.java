package com.dyc.course.sys.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysConfigEntity;

public interface SysConfigRepository extends JpaRepository<SysConfigEntity, Long> {

	Page<SysConfigEntity> findByParamKeyLike(String paramKey, Pageable pageable);
	
}
