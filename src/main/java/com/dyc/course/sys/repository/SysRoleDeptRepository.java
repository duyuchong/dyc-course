package com.dyc.course.sys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysRoleDeptEntity;

public interface SysRoleDeptRepository extends JpaRepository<SysRoleDeptEntity, Long> {

}
