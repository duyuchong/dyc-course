package com.dyc.course.sys.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysDictEntity;

public interface SysDictRepository extends JpaRepository<SysDictEntity, Long> {

	Page<SysDictEntity> findByNameLike(String name, Pageable pageable);
	
}
