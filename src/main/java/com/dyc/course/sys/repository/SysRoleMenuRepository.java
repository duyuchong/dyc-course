package com.dyc.course.sys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.dyc.course.sys.entity.SysRoleMenuEntity;

public interface SysRoleMenuRepository extends JpaRepository<SysRoleMenuEntity, Long> {

	@Transactional
	void deleteByMenuId(Long menuId);
	
}
