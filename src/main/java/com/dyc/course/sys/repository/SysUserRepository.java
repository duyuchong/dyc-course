package com.dyc.course.sys.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dyc.course.sys.entity.SysUserEntity;

public interface SysUserRepository extends JpaRepository<SysUserEntity, Long> {

	Page<SysUserEntity> findByUsernameLike(String username, Pageable pageable);
	
	List<SysUserEntity> findByUsername(String username);
	
}
