package com.dyc.course.sys.service;

import com.dyc.course.sys.entity.SysRoleEntity;

/**
 * 角色
 *
 */
public interface SysRoleService {

	void saveRole(SysRoleEntity role);

	void update(SysRoleEntity role);
	
	void deleteBatch(Long[] roleIds);

}
