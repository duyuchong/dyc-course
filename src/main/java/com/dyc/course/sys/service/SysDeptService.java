package com.dyc.course.sys.service;

import java.util.List;
import java.util.Map;

import com.dyc.course.sys.entity.SysDeptEntity;

/**
 * 部门管理
 *
 */
public interface SysDeptService {

	List<SysDeptEntity> queryList(Map<String, Object> map);

	/**
	 * 查询子部门ID列表
	 * @param parentId  上级部门ID
	 */
	List<Long> queryDetpIdList(Long parentId);

	/**
	 * 获取子部门ID，用于数据过滤
	 */
	List<Long> getSubDeptIdList(Long deptId);

}
