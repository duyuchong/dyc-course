package com.dyc.course.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dyc.course.sys.entity.SysRoleMenuEntity;
import com.dyc.course.sys.mapper.SysRoleMenuMapper;
import com.dyc.course.sys.repository.SysRoleMenuRepository;
import com.dyc.course.sys.service.SysRoleMenuService;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 */
@Service
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Autowired
	private SysRoleMenuMapper mapper;

    @Autowired
	private SysRoleMenuRepository repository;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
		//先删除角色与菜单关系
		mapper.deleteBatch(new Long[]{roleId});

		if (menuIdList.size() == 0) {
			return ;
		}

		//保存角色与菜单关系
		for (Long menuId : menuIdList) {
			SysRoleMenuEntity sysRoleMenuEntity = new SysRoleMenuEntity();
			sysRoleMenuEntity.setMenuId(menuId);
			sysRoleMenuEntity.setRoleId(roleId);

			repository.save(sysRoleMenuEntity);
		}
	}

}
