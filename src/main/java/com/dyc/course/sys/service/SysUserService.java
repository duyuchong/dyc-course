package com.dyc.course.sys.service;

import com.dyc.course.sys.entity.SysUserEntity;

/**
 * 系统用户
 *
 */
public interface SysUserService {
	
	/**
	 * 保存用户
	 */
	void saveUser(SysUserEntity user);
	
	/**
	 * 修改用户
	 */
	void update(SysUserEntity user);

	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	boolean updatePassword(Long userId, String password, String newPassword);
	
}
