package com.dyc.course.sys.service;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 */
public interface SysUserRoleService {
	
	void saveOrUpdate(Long userId, List<Long> roleIdList);

}
