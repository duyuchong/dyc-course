package com.dyc.course.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dyc.course.sys.entity.SysUserRoleEntity;
import com.dyc.course.sys.repository.SysUserRoleRepository;
import com.dyc.course.sys.service.SysUserRoleService;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 */
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {
	
	@Autowired
	private SysUserRoleRepository repository;
	
	@Override
	public void saveOrUpdate(Long userId, List<Long> roleIdList) {
		//先删除用户与角色关系
		repository.deleteByUserId(userId);

		if (roleIdList == null || roleIdList.size() == 0) {
			return ;
		}
		
		//保存用户与角色关系
		for (Long roleId : roleIdList) {
			SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
			sysUserRoleEntity.setUserId(userId);
			sysUserRoleEntity.setRoleId(roleId);

			repository.save(sysUserRoleEntity);
		}

	}

}
