package com.dyc.course.sys.service;

import java.util.List;

/**
 * 角色与部门对应关系
 *
 */
public interface SysRoleDeptService {
	
	void saveOrUpdate(Long roleId, List<Long> deptIdList);
	
}
