package com.dyc.course.sys.service;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 */
public interface SysRoleMenuService {
	
	void saveOrUpdate(Long roleId, List<Long> menuIdList);
	
}
