package com.dyc.course.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dyc.course.sys.entity.SysRoleDeptEntity;
import com.dyc.course.sys.mapper.SysRoleDeptMapper;
import com.dyc.course.sys.repository.SysRoleDeptRepository;
import com.dyc.course.sys.service.SysRoleDeptService;

import java.util.List;

/**
 * 角色与部门对应关系
 *
 */
@Service
public class SysRoleDeptServiceImpl implements SysRoleDeptService {

    @Autowired
	private SysRoleDeptRepository repository;

    @Autowired
	private SysRoleDeptMapper mapper;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> deptIdList) {
		//先删除角色与部门关系
		mapper.deleteBatch(new Long[]{roleId});

		if (deptIdList.size() == 0) {
			return ;
		}

		//保存角色与菜单关系
		for (Long deptId : deptIdList) {
			SysRoleDeptEntity sysRoleDeptEntity = new SysRoleDeptEntity();
			sysRoleDeptEntity.setDeptId(deptId);
			sysRoleDeptEntity.setRoleId(roleId);

			repository.save(sysRoleDeptEntity);
		}
	}

}
