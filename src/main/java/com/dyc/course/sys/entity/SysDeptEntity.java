package com.dyc.course.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 部门管理
 *
 */
@Data
@Entity
@Table(name = "sys_dept")
public class SysDeptEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long deptId;
	
	/**
	 * 上级部门ID，一级部门为0
	 */
	private Long parentId;
	
	/**
	 * 部门名称
	 */
	private String name;
	
	/**
	 * 上级部门名称
	 */
	@Transient
	private String parentName;
	
	private Integer orderNum;
	
	private Integer delFlag;
	
	/**
	 * ztree属性
	 */
	@Transient
	private Boolean open;
	
	@Transient
	private List<?> list;
	
}
