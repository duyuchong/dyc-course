package com.dyc.course.sys.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 数据字典
 *
 */
@Data
@Entity
@Table(name = "sys_dict")
public class SysDictEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 字典名称
	 */
	@NotBlank(message="字典名称不能为空")
	private String name;
	
	/**
	 * 字典类型
	 */
	@NotBlank(message="字典类型不能为空")
	private String type;
	
	/**
	 * 字典码
	 */
	@NotBlank(message="字典码不能为空")
	private String code;
	
	/**
	 * 字典值
	 */
	@NotBlank(message="字典值不能为空")
	private String value;
	
	/**
	 * 排序
	 */
	private Integer orderNum;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 删除标记  -1：已删除  0：正常
	 */
	private Integer delFlag;

}
