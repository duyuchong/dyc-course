package com.dyc.course.sys.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * 系统配置信息
 *
 */
@Data
@Entity
@Table(name = "sys_config")
public class SysConfigEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message="参数名不能为空")
	private String paramKey;
	
	@NotBlank(message="参数值不能为空")
	private String paramValue;
	
	private String remark;

}
